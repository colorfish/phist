#include "phist_config.h"

#ifdef PHIST_HAVE_MPI
#include <mpi.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#ifdef PHIST_HAVE_LIKWID
#include "likwid.h"
#endif

#include "phist_macros.h"
#include "phist_enums.h"
#include "phist_kernels.h"
#include "phist_operator.h"

#include "phist_ScalarTraits.hpp"

#include ${PHIST_TG_HFILE}
#include "phist_driver_utils.h"

typedef phist::ScalarTraits<ST> st;
typedef phist::ScalarTraits<MT> mt;

//! benchmark the function mvec_QR, given k, n, m
//! do k times: randomize an n x m mvec and orthogonalize
//! it using mvec_QR.
int main(int argc, char** argv)
{
  int ierr;

  comm_ptr_t comm = NULL;
  map_ptr_t map = NULL;
  mvec_ptr_t X = NULL, Y=NULL;
  sdMat_ptr_t M = NULL;
  
  PHIST_ICHK_IERR(phist_kernels_init(&argc,&argv,&ierr),ierr);

PHIST_MAIN_TASK_BEGIN

  PHIST_ICHK_IERR(phist_comm_create(&comm,&ierr),ierr);

  if (argc!=4 && argc!=5)
  {
    PHIST_SOUT(PHIST_ERROR,"Usage: %s n m k [high_prec]\n"
                           "       perform k times: \n"
                           "    * vector_block dot itself\n"
                           " where n is the number of rows and m is the number of columns\n"
                           " set high_prec to 1 to request higher precision calculations!\n",
                           argv[0]);
    return 1;
  }

  gidx_t n;
  int m,k;
  if (sizeof(gidx_t)==4)
  {
    n=atoi(argv[1]);
  }
  else
  {
    n=atol(argv[1]);
  }
  
  m=atoi(argv[2]);
  k=atoi(argv[3]);
  bool high_prec = false;
  if( argc == 5 )
    high_prec = atoi(argv[4]);

  PHIST_ICHK_IERR(phist_map_create(&map,comm,n,&ierr),ierr);
  PHIST_ICHK_IERR(SUBR(mvec_create)(&X,map,m,&ierr),ierr);
  PHIST_ICHK_IERR(SUBR(sdMat_create)(&M,m,m,comm,&ierr),ierr);
  
  for (int it=0; it<k; it++)
  {
    ST value[m];
    if( high_prec )
      ierr = PHIST_ROBUST_REDUCTIONS;
    else
      ierr = 0;
    PHIST_ICHK_IERR(SUBR(mvecT_times_mvec)(st::one(),X,X,st::zero(),M,&ierr),ierr);
  }
  PHIST_ICHK_IERR(SUBR(sdMat_delete)(M,&ierr),ierr);
  PHIST_ICHK_IERR(SUBR(mvec_delete)(X,&ierr),ierr);
  PHIST_ICHK_IERR(phist_map_delete(map,&ierr),ierr);

PHIST_MAIN_TASK_END

  PHIST_ICHK_IERR(phist_kernels_finalize(&ierr),ierr);

  return ierr;
}
