
#include "phist_mpi_kernels.h"
#include "phist_ScalarTraits.hpp"

#ifdef PHIST_HAVE_SP
#include "phist_gen_s.h"
#include "phist_mpi_kernels_def.hpp"

#include "phist_gen_c.h"
#include "phist_mpi_kernels_def.hpp"
#endif

#include "phist_gen_d.h"
#include "phist_mpi_kernels_def.hpp"

#include "phist_gen_z.h"
#include "phist_mpi_kernels_def.hpp"
