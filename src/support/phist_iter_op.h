#ifndef PHIST_KRYLOV_AS_OPERATOR_H
#define PHIST_KRYLOV_AS_OPERATOR_H

#include "phist_config.h"
#include "phist_operator.h"
#include "phist_enums.h"

#ifdef PHIST_HAVE_SP
#include "phist_gen_s.h"
#include "phist_iter_op_decl.h"
#include "phist_gen_c.h"
#include "phist_iter_op_decl.h"
#endif

#include "phist_gen_d.h"
#include "phist_iter_op_decl.h"
#include "phist_gen_z.h"
#include "phist_iter_op_decl.h"
#include "phist_gen_clean.h"

#endif
